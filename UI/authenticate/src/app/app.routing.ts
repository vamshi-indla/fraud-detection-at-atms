import { NgModule, ModuleWithProviders } from "@angular/core";
 import { Routes, RouterModule } from "@angular/router";

 
 const routes: Routes = [
 ];
 
export const ModuleRouting: ModuleWithProviders = RouterModule.forRoot(routes);
