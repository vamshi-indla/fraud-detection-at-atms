import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { NgModule, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { UserService } from './user-service';
import { Global } from './globals';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import {AtomSpinnerModule} from 'angular-epic-spinners'
import { HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TemplateDrivenFormComponent } from './template-driven-form.component';
@NgModule({
  imports:[
    CommonModule,
    NgtUniversalModule,  
    FormsModule,
    ReactiveFormsModule,
    CreditCardDirectivesModule,
    FormsModule,
    AtomSpinnerModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [
      AppComponent,
      TemplateDrivenFormComponent	  
  ],
  providers: [
      UserService,
      Global
  ],
})
export class AppModule { }
