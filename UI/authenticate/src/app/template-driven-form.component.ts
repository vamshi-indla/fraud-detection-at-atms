import { Component, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from './user-service';
import { User } from './user';
import { CreditCardValidator } from 'angular-cc-library';
import {  HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { headersToString } from 'selenium-webdriver/http';
import { Options } from 'selenium-webdriver/opera';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*");

@Component({
   selector: 'app-template',
   templateUrl: './template-driven-form.component.html',
   styleUrls: ['./app.component.css']
})
export class TemplateDrivenFormComponent implements OnInit {
  cardnumPattern = "^[0-9]{16}$"; 
  form: FormGroup;
  public submitted;
  public message;
  public atomTitle = true;
  public loading = false;
  display = false;

  user = new User();
  
  @ViewChild("video")
  public video: ElementRef;

  @ViewChild("canvas")
  public canvas: ElementRef;

  public captures: Array<any>;

  constructor(private userService: UserService, private _fb: FormBuilder, 
    private http: HttpClient, private spinnerService: Ng4LoadingSpinnerService) {
    this.captures = [];
  }
  ngOnInit() {
    this.form = this._fb.group({
      creditCard: ['', [<any>CreditCardValidator.validateCCNumber]],
     });
  }
  
  public ngAfterViewInit() {
    console.log("call0");
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            this.video.nativeElement.src = window.URL.createObjectURL(stream);
            this.video.nativeElement.play();
        });
    }

  }

  public capture() {
      this.spinnerService.show();
      console.log(this.user.cardNumber);
      this.captures.pop();
      var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 640, 480);
      this.captures.push(this.canvas.nativeElement.toDataURL("image/png"));
      console.log(this.canvas.nativeElement);
      var formdata: FormData = new FormData();
      
      formdata.append('cardno', this.user.cardNumber);
      console.log(formdata.get('File'))
      
      console.log(formdata);
      var file = this.dataURLtoFile(this.canvas.nativeElement.toDataURL("image/png"), '/users/vkuruva/test.png');
      formdata.append('file',  file);

      this.submitted = 
        this.http.post
        ('http://facerecognition-env.5vhmgwp3r7.us-east-1.elasticbeanstalk.com/Authenticate/upload'
        , formdata
        , {headers: {'Accept': 'text/plain'}
        , responseType: 'text'
      }
      )
        .subscribe(responsedata => {
          
          this.message = responsedata;
          console.log(responsedata);
          this.spinnerService.hide();

        },
        err => { 
          this.message = 'Exception: ' + err.error.text;
          console.log(err);
          
        }
      );
  }

  public dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
}
}
