import { Component, OnInit, ViewChild, ElementRef, Injectable, NgModule } from '@angular/core';
import { NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from './user-service';
import { User } from './user';
import { CreditCardValidator } from 'angular-cc-library';
import {  HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { headersToString } from 'selenium-webdriver/http';
import { Options } from 'selenium-webdriver/opera';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*");

@NgModule({
  imports: [ BrowserModule ],
  declarations: [ SignupComponent ],
})


@Component({
   selector: 'app-template',
   templateUrl: './signup.html',
   styleUrls: ['./app.component.css']
})
export class SignupComponent implements OnInit {
  cardnumPattern = "^[0-9]{16}$"; 
  mobilenumPattern = "";
  fullname = "";
  form: FormGroup;
  public submitted;
  public message;
  public atomTitle = true;
  public loading = false;
  display = false;

  public user = new User();
  
  @ViewChild("video")
  public video: ElementRef;

  @ViewChild("canvas")
  public canvas: ElementRef;

  public captures: Array<any>;

  constructor(private userService: UserService, private _fb: FormBuilder, 
    private http: HttpClient, private spinnerService: Ng4LoadingSpinnerService) {
    this.captures = [];
  }
  ngOnInit() {
    this.form = this._fb.group({
      creditCard: ['', [<any>CreditCardValidator.validateCCNumber]],
     });
  }
  
  public ngAfterViewInit() {
    console.log("call0");
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            this.video.nativeElement.src = window.URL.createObjectURL(stream);
            this.video.nativeElement.play();
        });
    }

  }

  public capture() {
      this.spinnerService.show();
      this.captures.pop();
      var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 640, 480);
      this.captures.push(this.canvas.nativeElement.toDataURL("image/png"));
      console.log(this.canvas.nativeElement);
      var formdata: FormData = new FormData();
      

      console.log(formdata.get('File'))
      
      console.log(formdata);
      var file = this.dataURLtoFile(this.canvas.nativeElement.toDataURL("image/png"), '/users/vkuruva/test.png');

      formdata.append('cardno', this.user.cardNumber);
      formdata.append('fullname', this.user.fullname);
      formdata.append('mobileno', this.user.mobilenumber);      
      formdata.append('file',  file);
      
      this.submitted = 
        this.http.post
        ('http://facerecognition-env.5vhmgwp3r7.us-east-1.elasticbeanstalk.com/register/userdetails'
        , formdata
        , {headers: {'Accept': 'text/plain'}
        , responseType: 'text'
      }
      )
        .subscribe(responsedata => {
          this.spinnerService.hide();
          this.message = responsedata;
          console.log(responsedata);
        },
        err => { 
          this.message = 'Exception: ' + err.error.text;
          console.log(err);
          this.spinnerService.hide();
        }
      );
  }
  public dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
}
}
