package com.cg.aws.facerek.controller;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import com.amazonaws.services.rekognition.model.InvalidParameterException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.*;
import com.cg.aws.facerek.Domain.Authentication;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.Jedis;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/Authenticate")
@CrossOrigin
public class AuthenticationController {

    /* Local AWS
    private static final String dynamoDBTable = "customer_collection";
    private static final String faceCollection = "family_collection";
    private static final String bucket = "image-recognition-0517";
    private static final String access_key = "AKIAJU4BZQPEDK4KLWUA";
    private static final String secret_access_key = "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";*/

    //CG AI4 AWS
    private static final String customerCollection = "customer_collection";
    private static final String faceCollection = "face_collection";
    private static final String bucket = "face-auth-store";
    private static final String access_key = "AKIAJNZCHF5BL5MS663A";
    private static final String secret_access_key = "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";

    @RequestMapping(value="/userInputDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String userInputDetails(Authentication authentication)
    {
        String returnString = "Authorization Failed";

        try
        {

            AWSCredentials credentials = new BasicAWSCredentials(access_key, secret_access_key);

            ClientConfiguration clientCfg = new ClientConfiguration();
            clientCfg.setProtocol(Protocol.HTTPS);
            clientCfg.setConnectionTimeout(1200000);
            clientCfg.setSocketTimeout(1200000);
            clientCfg.setMaxErrorRetry(10);
            clientCfg.setMaxConnections(10);

            AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder
                    .standard()
                    .withRegion(Regions.US_EAST_1)
                    .withClientConfiguration(clientCfg)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();

            DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);
            Table table = dynamoDB.getTable(customerCollection);

            QuerySpec spec = new QuerySpec()
                    .withKeyConditionExpression("CardNo = :cardNoValue")
                    .withValueMap(new ValueMap()
                            .withString(":cardNoValue", authentication.getCardNo()));

            ItemCollection<QueryOutcome> items = table.query(spec);
            Iterator<Item> iterator = items.iterator();

            if (iterator.hasNext()) {
                Item item = iterator.next();
                System.out.println("itemValueFromDB--->" + item);

                String CardRekognitionId = item.get("RekognitionId").toString();
                System.out.println("Card Rekognition Id--->" + CardRekognitionId);

                String associatedMobileNo = item.get("MobileNo").toString();
                //associatedMobileNo = "+1"+associatedMobileNo;
                System.out.println("Associated Mobile Number--->" + associatedMobileNo);

                String fullName = item.get("FullName").toString();
                System.out.println("Full Name--->" + fullName);

                AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
                        .standard()
                        .withRegion(Regions.US_EAST_1)
                        .withClientConfiguration(clientCfg)
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .build();

                ObjectMapper objectMapper = new ObjectMapper();
                System.out.println("Image path in S3--->" + authentication.getS3ImagePath());

                Image image = new Image()
                        .withS3Object(new S3Object()
                                .withBucket(bucket)
                                .withName(authentication.getS3ImagePath()));

                System.out.println(bucket + "-----" + image);

                SearchFacesByImageRequest searchFacesByImageRequest = new SearchFacesByImageRequest()
                        .withCollectionId(faceCollection)
                        .withImage(image)
                        .withFaceMatchThreshold(90F)
                        .withMaxFaces(1);

                try {
                    SearchFacesByImageResult searchFacesByImageResult =
                            rekognitionClient.searchFacesByImage(searchFacesByImageRequest);

                    System.out.println("Faces matching largest face in image from " + authentication.getS3ImagePath());
                    List<FaceMatch> faceImageMatches = searchFacesByImageResult.getFaceMatches();
                    if (faceImageMatches != null && faceImageMatches.size() > 0) {
                        for (FaceMatch face : faceImageMatches) {

                            System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
                            System.out.println();

                            ObjectMapper mapper = new ObjectMapper();
                            JsonNode rootElement = mapper.readTree(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
                            JsonNode faceNode = rootElement.get("face");
                            JsonNode faceIdNode = faceNode.get("faceId");
                            String FaceRekognitionId = faceIdNode.toString();
                            FaceRekognitionId = FaceRekognitionId.substring(1, FaceRekognitionId.length() - 1);
                            System.out.println("FaceRekognitionId--->" + FaceRekognitionId);

                            dynamoDB = new DynamoDB(amazonDynamoDB);
                            table = dynamoDB.getTable(faceCollection);

                            spec = new QuerySpec()
                                    .withKeyConditionExpression("RekognitionId = :RekognitionIdValue")
                                    .withValueMap(new ValueMap()
                                            .withString(":RekognitionIdValue", FaceRekognitionId));

                            items = table.query(spec);
                            iterator = items.iterator();

                            if (iterator.hasNext()) {
                                item = iterator.next();
                                String CardNumber = item.get("CardNo").toString();
                                System.out.println("Card No using FaceRekognitionId-->" + CardNumber);
                                if (CardNumber != null && CardNumber.equalsIgnoreCase(authentication.getCardNo())) {
                                    System.out.println("CardRekognitionId matching with FaceRekognitionId");
                                    returnString = "Authorization Successful";
                                    System.out.println("Authenticated");
                                    return returnString;
                                } else {
                                    System.out.println("CardRekognitionId not matching with FaceRekognitionId");
                                    returnString = "NotAbleToMatchUsingFace";
                                }
                            } else {
                                System.out.println("CardRekognitionId not matching with FaceRekognitionId");
                                returnString = "NotAbleToMatchUsingFace";
                            }

                            if (returnString.equalsIgnoreCase("Authorization Successful")) {
                                break;
                            }

                        }
                    }

                    if ((faceImageMatches == null || faceImageMatches.size() == 0) || returnString.equalsIgnoreCase("NotAbleToMatchUsingFace")) {
                        SMSValidation(associatedMobileNo, fullName, authentication);
                    }
                } catch (InvalidImageFormatException e) {
                    System.out.println("Invalid image encoding");
                    returnString = SMSValidation(associatedMobileNo,fullName, authentication);
                    e.printStackTrace();
                } catch (InvalidParameterException e) {
                    System.out.println("System processing error - There are no faces in the image. Should be at least 1");
                    returnString = SMSValidation(associatedMobileNo,fullName, authentication);
                    e.printStackTrace();
                } catch (Exception e) {
                    System.out.println("System processing error");
                    returnString = SMSValidation(associatedMobileNo,fullName, authentication);
                    e.printStackTrace();
                }
            } else {
                System.out.println("UnAuthenticated - InValidCardNumber");
                returnString = "Invalid Card Number";
            }

            return returnString;
        } catch(Exception e){
            e.printStackTrace();
            return "Authorization Failed";
        }
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(
            @RequestParam("file") MultipartFile file, @RequestParam String cardno)
    {
        System.out.println("Authentication Method Started");

        long unixTime = System.currentTimeMillis() / 1000L;
        String tempName = cardno+"_"+unixTime;

        String imageName  = Base64.getEncoder().encodeToString(tempName.getBytes())+".jpg";
        System.out.println("imageName--->"+imageName);

        if (file != null && !file.isEmpty())
        {
            try
            {
                byte[] bytes = file.getBytes();
                System.out.println("Before writing into file");

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("/var/app/tmp/"+imageName)));
                stream.write(bytes);
                stream.close();
                System.out.println("After writing into file");

                AWSCredentials credentials = new BasicAWSCredentials(access_key,secret_access_key);

                ClientConfiguration clientCfg = new ClientConfiguration();
                clientCfg.setProtocol(Protocol.HTTPS);
                clientCfg.setConnectionTimeout(50000);

                AmazonS3 s3client = new AmazonS3Client(credentials,clientCfg);

                File fileuploaded = new File("/var/app/tmp/"+imageName);
                System.out.println("File uploaded into local path-->"+fileuploaded.getName());

                PutObjectResult putObjectResult = s3client.putObject(new PutObjectRequest(bucket, fileuploaded.getName(), fileuploaded)
                        .withCannedAcl(CannedAccessControlList.BucketOwnerFullControl));

                System.out.println("File uploaded into S3-->"+putObjectResult.getMetadata());
                fileuploaded.delete();

                Authentication authentication = new Authentication();
                authentication.setCardNo(cardno);
                authentication.setS3ImagePath(fileuploaded.getName());

                String returnString = userInputDetails(authentication);
                return returnString;

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("System error");
                return "Authorization Failed" ;

            }
        } else {
            return "Image is not received";
        }
    }

    public String SMSValidation(String associatedMobileNo, String fullName, Authentication authentication)
    {

        String returnString = "";

        System.out.println("Two way authentication initiated");

        AmazonSNSClient snsClient = new AmazonSNSClient(new BasicAWSCredentials(access_key, secret_access_key));
        snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));

        CreateTopicRequest createTopicRequest = new CreateTopicRequest("TopicForSendSMS");
        CreateTopicResult createTopicResult = snsClient.createTopic(createTopicRequest);

        //print TopicArn
        System.out.println("result-->"+createTopicResult);
        //get request id for CreateTopicRequest from SNS metadata
        System.out.println("CreateTopicRequest - " + snsClient.getCachedResponseMetadata(createTopicRequest));

        String topicArn = createTopicResult.toString().substring(11,createTopicResult.toString().length()-1);
        System.out.println("topicArn--->"+topicArn);

        Jedis jedis = new Jedis("facerekresponse.3dbnaj.ng.0001.use1.cache.amazonaws.com", 6379);
        Long deletedValue = jedis.del(associatedMobileNo);
        System.out.println("deletedValue--->" + deletedValue);

        SubscribeRequest subRequest = new SubscribeRequest(topicArn, "sms", associatedMobileNo);
        snsClient.subscribe(subRequest);

        String messageToCustomer = "FREE TEXT from ATM Authentication: Message for "+fullName+" re: credit card ending with #"+authentication.getCardNo().substring(authentication.getCardNo().length()-4);
        System.out.println("messageToCustomer---->"+messageToCustomer);
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setMessage(messageToCustomer);
        publishRequest.setPhoneNumber(associatedMobileNo);
        PublishResult publishResult = snsClient.publish(publishRequest);

        System.out.println("publish Result after sent sms--->"+publishResult);
        System.out.println("Sent SMS to performa two step authentication--->"+associatedMobileNo);
        returnString = "Authorization Failed";

        try {

            final long NANOSEC_PER_SEC = 1000l * 1000 * 1000;

            long startTime = System.nanoTime();
            System.out.println("startTime--->" + startTime);
            while ((System.nanoTime() - startTime) < 1 * 58 * NANOSEC_PER_SEC) {

                System.out.println(((String) ("Time difference--->" + (System.nanoTime() - startTime))));

                if (jedis.get(associatedMobileNo) != null) {
                    System.out.println("Message received from "+associatedMobileNo);
                    String messageResponse = jedis.get(associatedMobileNo);
                    System.out.println("messageResponse--->" + messageResponse);
                    if (messageResponse != null && messageResponse.contains("Yes")) {
                        System.out.println("Authenticated");
                        returnString = "Authorization Successful";
                    } else {
                        System.out.println("UnAuthenticated");
                        returnString = "Authorization Failed";
                    }
                    deletedValue = jedis.del(associatedMobileNo);
                    System.out.println("deletedValue--->" + deletedValue);
                    break;
                } else {
                    try {
                        System.out.println("sleeping for 2 seconds");
                        Thread.sleep(2 * 1000);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
            if(jedis != null && jedis.isConnected()) {
                jedis.close();
            }
        } catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
            System.out.println("System Unavailable");
            returnString = "Authorization Failed";
        }

        return returnString;

    }
}
