package com.cg.aws.facerek.Domain;

import org.springframework.web.multipart.MultipartFile;

public class Authentication {

    private String cardNo;
    private String s3ImagePath;
    private String mobileNumber;
    private String fullName;
    private MultipartFile multipartFile;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getS3ImagePath() {
        return s3ImagePath;
    }

    public void setS3ImagePath(String s3ImagePath) {
        this.s3ImagePath = s3ImagePath;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
