package com.cg.aws.facerek.controller;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.services.rekognition.model.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@RestController
@RequestMapping("/register")
@CrossOrigin
public class RegistrationController {

    private static final String dynamoDBTable = "customer_collection";
    private static final String faceCollection = "face_collection";
    private static final String bucket = "face-rekognition-0101";
    private static final String access_key = "AKIAJNZCHF5BL5MS663A";
    private static final String secret_access_key = "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";

    @RequestMapping(value="/userdetails", method= RequestMethod.POST)
    public @ResponseBody
    String handleFileUpload(
            @RequestParam("file") MultipartFile file, @RequestParam String cardno,@RequestParam String mobileno,@RequestParam String fullname)
    {
        System.out.println("Registration Started");

        String result = "";
        String rekognitionId = "";

        long unixTime = System.currentTimeMillis() / 1000L;

        if(cardno == null  || (cardno != null && cardno.equalsIgnoreCase(""))){
            result = "Card Number is not received";
            System.out.println(result);
            return result;
        } else if(!cardno.matches("^[0-9]*$")){
            result = "Invalid Card Number";
            System.out.println(result);
            return result;
        }

        if (mobileno == null  || (mobileno != null && mobileno.equalsIgnoreCase(""))){
            result = "Mobile Number is not received";
            System.out.println(result);
            return result;
        } else if(!mobileno.matches("^[0-9+]*$")){
            result = "Invalid Mobile Number";
            System.out.println(result);
            return result;
        }

        if(fullname == null  || (fullname != null && fullname.equalsIgnoreCase(""))) {
            result = "Full Name is not received";
            System.out.println(result);
            return result;
        } else if(!fullname.matches("^[a-zA-Z0-9_ ]*$")){
            result = "Invalid Full Name";
            System.out.println(result);
            return result;
        }

        else if (file != null && !file.isEmpty()) {
            try {

                String tempName = cardno+"_"+fullname.replace(" ","")+"_"+unixTime;

                String imageName = tempName + ".jpg";

                System.out.println("Before writing into file");
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("/var/app/tmp/"+imageName)));
                //BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("/Users/sudhakarangovindaraj/Desktop/"+imageName)));
                stream.write(bytes);
                stream.close();
                System.out.println("After writing into file");

                AWSCredentials credentials = new BasicAWSCredentials(access_key,secret_access_key);

                ClientConfiguration clientCfg = new ClientConfiguration();
                clientCfg.setProtocol(Protocol.HTTP);
                clientCfg.setConnectionTimeout(50000);

                AmazonS3 s3client = new AmazonS3Client(credentials,clientCfg);
                File fileuploaded = new File("/var/app/tmp/"+imageName);
                //File fileuploaded = new File("/Users/sudhakarangovindaraj/Desktop/"+imageName);
                System.out.println("File uploaded into local path-->"+fileuploaded.getName());

                PutObjectResult putObjectResult = s3client.putObject(new PutObjectRequest(bucket, fileuploaded.getName(), fileuploaded)
                        .withCannedAcl(CannedAccessControlList.BucketOwnerFullControl));
                System.out.println("File uploaded into S3--->"+putObjectResult.getMetadata());
                fileuploaded.delete();

                clientCfg = new ClientConfiguration();
                clientCfg.setProtocol(Protocol.HTTPS);
                clientCfg.setConnectionTimeout(1200000);
                clientCfg.setSocketTimeout(1200000);
                clientCfg.setMaxErrorRetry(10);
                clientCfg.setMaxConnections(10);

                AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
                        .standard()
                        .withRegion(Regions.US_EAST_1)
                        .withClientConfiguration(clientCfg)
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .build();

                Image image = new Image()
                        .withS3Object(new S3Object()
                                .withBucket(bucket)
                                .withName(fileuploaded.getName()));

                IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                        .withImage(image)
                        .withCollectionId(faceCollection);

                IndexFacesResult indexFacesResult = rekognitionClient.indexFaces(indexFacesRequest);

                System.out.println("Results for " + fileuploaded.getName());
                System.out.println("Faces indexed:");
                List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
                for (FaceRecord faceRecord : faceRecords) {
                    System.out.println("  Face ID: " + faceRecord.getFace().getFaceId());
                    if(faceRecord.getFace().getFaceId() != null) {
                        rekognitionId = faceRecord.getFace().getFaceId();
                    }
                    System.out.println("  Location:" + faceRecord.getFaceDetail().getBoundingBox().toString());
                }

                if(faceRecords.size() > 0 && rekognitionId != null && !rekognitionId.equalsIgnoreCase("")) {
                    AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder
                            .standard()
                            .withRegion(Regions.US_EAST_1)
                            .withClientConfiguration(clientCfg)
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .build();

                    DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

                    Table table = dynamoDB.getTable(dynamoDBTable);
                    Item item = new Item()
                            .withPrimaryKey("CardNo", cardno)
                            .withString("FullName", fullname)
                            .withString("MobileNo", mobileno)
                            .withString("RekognitionId", rekognitionId);

                    PutItemOutcome putItemOutcome = table.putItem(item);
                    System.out.println("Updated in customer collection DynamoDB--->"+putItemOutcome.toString());

                    table = dynamoDB.getTable(faceCollection);
                    item = new Item()
                            .withPrimaryKey("RekognitionId", rekognitionId)
                            .withString("CardNo", cardno);
                    putItemOutcome = table.putItem(item);
                    System.out.println("Updated in face collection DynamoDB--->"+putItemOutcome.toString());

                    result = "Image is successfully uploaded";
                    System.out.println(result);
                } else{
                    result = "Face Index Error";
                    System.out.println(result);
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Insertion Failed into DynamoDB");
                return "Upload Error";

            }
        } else {
            System.out.println("Image not received");
            return "Image is not received";
        }
    }
}
