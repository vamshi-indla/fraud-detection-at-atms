
# Project Title: Fraud Detection at ATMs

# Team Members
- Vamshi Krishna Indla
- Vamsi Kuruva
- Kiran Pilli
- Andrews Samuel
- Sudhakaran Govidarajan

# Description of the project

In 2017, the number of debit cards compromised at ATM and merchant card readers rose 10 percent. A typical fraud at ATM is done through Skimming, a process that involves collecting victim's card information & PIN and then drawing cash from their accounts. 
Skimming accounts for $1 billion a year fraud.

# Solution 

- “Fraud detection at ATMs” is a project implemented using Advanced AI services offered by AWS  Rekognition, Pinpoint and SNS that does Face Match and Multi-factor authentication using two-way Mobile SMS. 
- Multiple other AWS services are integrated to form a Secure and robust  solution to curb fraudulent transactions of skimmed cards at ATM. 
- Solution is designed to easily integrate with existing Software of Financial Organization’s ATM.

# Solution Potential

- Identify Persons of Interest for Law Enforcement Agencies
- Whitelisting a group of people for a VIP experience

## Features
- Real Time Enrollment of Card Holder and his Face image. Its REST API is powered by AWS Elastic Bean
- Phone Number validation using AWS Pinpoint services
- Simulated ATM setup to test the Cardholder Authentication and Multi factor authentication using AWS Pinpoint two-way SMS serviceom/)


## Architecture 

### Realtime Enrollment
![Realtime Enrollment](/images/realtimeenrollment.png?raw=true)


### Cardholder Authentication
![Cardholder Authentication](/images/cardholderauthentication.PNG?raw=true)


## Stack
- Angular6 - (Simulated ATM)
- Java8 with Springboot - (Backend code) 
- Python - (Bulk Enrollment)
- AWS S3 - (Static Website Host)
- AWS Rekognition API - (Image processing, Indexing faces and facematch)
- AWS Pinpoint & SNS - (two way sms for Multifactor authentication)
- AWS Lambda
- AWS Beanstack
- AWS Redis - (Caching)
- AWS Route 53 - (Register Domain)
- AWS API Gateway
- Camcorder - (Video recording)

## Installation Steps

1.	Create new S3 bucket face-rekognition-0101 which will be used to save the User Registration images
2.	Create new S3 bucket face-auth-store which will be used to save the images which is captured in the camera
3.	Create customer_collection table in DynamoDB with below columns
  a.	CardNo – Partition key
  b.	FullName
  c.	MobileNo
  d.	RekognitionId
4.	Create face_collection table in DynamoDB with below columns
  a.	RekognitionId – Partition key
  b.	CardNo
5.	Create face_collection in Rekognition module to save the face images. 
  a.	After saving the face images, RekognitionId will be returned from AWS function and its will be saved into DynamoDB tables
6.	Create a war file from server-side-code folder (Written in Spring boot) and deploy the same into Elastic Beanstalk
7.	Write a Lambda function to copy the pin point response messages to Redis

## Link to the video demonstration
![Video(/video/frauddetectionatatms.mp4)

## References
- [Numvalidate documentation](https://github.com/apilayer/numvalidate/blob/master/README.md)